package com.example.examenrt;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class KafkaConsumerToCSV {

    public static void toCSV(String args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", IKafkaConstants.BROKER);
        props.put("group.id", IKafkaConstants.GROUP_ID);
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("auto.offset.reset", IKafkaConstants.OFFSET_RESET_EAELIER);

        try (Consumer<String, String> consumer = new KafkaConsumer<>(props);
             FileWriter writer = new FileWriter("consumer_output.csv")) {

            consumer.subscribe(Arrays.asList(IKafkaConstants.TOPIC));

            final int giveUp = 100;
            int noRecordsCount = 0;

            while (true) {
                final ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(100));

                if (consumerRecords.count() == 0) {
                    noRecordsCount++;
                    if (noRecordsCount > giveUp) break;
                    else continue;
                }

                for (ConsumerRecord<String, String> record : consumerRecords) {
                    writer.append(record.key())
                            .append(',')
                            .append(record.value())
                            .append('\n');
                }

                consumer.commitAsync();
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
