package com.example.examenrt;

public class Produit {
private String id;
    private String name;

    public Produit() {
    }

    public Produit(String id, String name, String description, String price, String quantity) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return "Data{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

}
