package com.example.examenrt;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Duration;
import java.util.Arrays;

@SpringBootApplication
public class ExamenrtApplication {

    public static void main(String[] args) {
//        SpringApplication.run(ExamenrtApplication.class, args);
        runConsumer();
    }
    public static void runConsumer() {
        Consumer consumer = Consomateur.createConsumer();
        consumer.subscribe(Arrays.asList(IKafkaConstants.TOPIC));

        while (true){
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record: records){
                KafkaConsumerToCSV.toCSV(record.value().toString());
            }
        }
    }
}
